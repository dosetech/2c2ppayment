﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using _2C2P_Dev.Models;
using _2C2P;
using System.Globalization;
using System.Web;

namespace _2C2P_Dev.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {


            return View();
        }

        [HttpGet]
        public IActionResult Redirect()
        {

            var model = new ModelForPayment
            {
                RequestModel = new RequestModel
                {
                    Version = "8.5",
                    MerchantId = "764764000002038",
                    PaymentDescription = "test",
                    UniqueTransactionCode = DateTime.Now.ToString("ssfff"),
                    Amount = (500.00 * 100).ToString("000000000000"), //"000000002500", // 25 บาท
                    SecretKey = "FD94B4F93C0AD8B244A081F3D3DD33D9317DA70E7242A2B4EEC7CF972F1A63F9",
                    result_url_1 = "http://localhost:62132/Home/ResultPayment",
                    PaymentOption = PaymentRedirectOptionCode.AllAvailableOptions,
                    payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment"
                }
            };
            model.RequestModel.HashValue = model.RequestModel.SetHashValueSha256(model.RequestModel.SetRedirectMessgae());
            return View(model);
        }

        public IActionResult Secure()
        {
            var model = new ModelForPayment
            {
                RequestModel = new RequestModel
                {
                    Version = "9.3",
                    TimeStamp = DateTime.Now.ToString("ddMMyyHHmmss"),
                    MerchantId = "764764000002038",
                    UniqueTransactionCode = DateTime.Now.ToString("ssfff"),
                    PaymentDescription = HttpUtility.HtmlEncode("test"),
                    Amount = (500.00 * 100).ToString("000000000000"), //"000000002500", // 25 บาท
                    SecretKey = "FD94B4F93C0AD8B244A081F3D3DD33D9317DA70E7242A2B4EEC7CF972F1A63F9",
                    PaymentChannel = PaymentSecureOptionCode.AlternativePaymentMethod,
                    AgentCode = AgentCode.BankKrungThai,
                    ChannelCode = APMChannelCode.BankCounter,
                    Option3DS = "N",
                    PaymentExpire = DateTime.Now.AddHours(1).ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US")),
                    MobileNo = "0909931579",
                    Email = "miromoll@hotmail.com",
                    result_url_1 = "http://localhost:62132/Home/ResultPayment",
                    payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/Payment.aspx"
                }
            };

            model.RequestModel.Message = model.RequestModel.SetSecureMessage();
            model.RequestModel.HashValue = model.RequestModel.SetHashValue(model.RequestModel.Message);
            model.RequestModel.Xml = new XmlService().SetXmlCode(model.RequestModel);

            //SecurePaymentMethod securePaymentMethod = new SecurePaymentMethod();
            //var result = securePaymentMethod.PaymentSecue(model.RequestModel.Xml, "C:/Users/Toanaki/source/repos/2C2P-Dev/2C2P/Cert/demo2.pfx");
            return View(model);
        }
        public IActionResult ResultPayment(PaymentResponseRedirect vm, string paymentResponse)
        {
            //internetbanking
            if (!String.IsNullOrEmpty(paymentResponse))
            {
                var resultPaymentSecure = new SecurePaymentMethod().DecodeResult(paymentResponse, "path2c2pcert");
                if (resultPaymentSecure.PaymentResponse.respCode == "000" || resultPaymentSecure.PaymentResponse.respCode == "00")
                {
                    //suscess
                }
                else if (resultPaymentSecure.PaymentResponse.respCode == "010")
                {
                    //cancel
                }
            }
            else
            {
                if (vm.channel_response_code == "00" || vm.channel_response_code == "000")
                {
                    //suscess
                }
                else if(vm.channel_response_code == "0036")
                {
                    //cancel
                }

            }
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
