﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace _2C2P
{
    public class XmlService
    {      
        public string SetXmlCode(RequestModel Request)
        {
            string Xml = "<PaymentRequest>" +
                          "<version>" + Request.Version + "</version>" +
                          "<timeStamp>" + Request.TimeStamp + "</timeStamp>" +
                          "<merchantID>" + Request.MerchantId + "</merchantID>" +
                          "<uniqueTransactionCode>" + Request.UniqueTransactionCode + "</uniqueTransactionCode>" +
                          "<desc>" + HttpUtility.HtmlEncode(Request.PaymentDescription) + "</desc>" +
                          "<amt>" + Request.Amount + "</amt>" +
                          "<currencyCode>" + Request.CurrencyCode + "</currencyCode>" +
                          "<paymentChannel>" + Request.PaymentChannel + "</paymentChannel>" +
                          "<panCountry>" + Request.Country + "</panCountry>" +
                           "<request3DS>" + Request.Option3DS + "</request3DS>" +
                           "<agentCode>" + Request.AgentCode + "</agentCode>" +
                          "<channelCode>" + Request.ChannelCode + "</channelCode>" +
                          "<paymentExpiry>" + Request.PaymentExpire + "</paymentExpiry>" +
                          "<mobileNo>" + Request.MobileNo + "</mobileNo>" +
                          "<cardholderEmail>" + Request.Email + "</cardholderEmail>" +
                          "<secureHash>" + Request.HashValue + "</secureHash>" +
                          "</PaymentRequest>";
            string Xml64 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Xml));
            //string signature = Request.SetHashValueSha256(Request.SetSecureMessage()).ToUpper();
            //var payloadXML = "<PaymentRequest>"+
            //                   "<version>" + Request.Version + "</version>" +
            //                   "<payload>"+ Xml64 + "</payload>" +
            //                   "<signature>"+ signature + "</signature>"+
            //                 "</PaymentRequest>";
            //string PayloadXml64 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(payloadXML));


            return Xml64;
        }
    }
}
