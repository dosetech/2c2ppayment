﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2C2P
{  
    public class APMChannelCode
    {
        public static readonly string ATM = "ATM";
        public static readonly string BankCounter  = "BANKCOUNTER";
        public static readonly string InternetBanking = "IBANKING";
        public static readonly string WebPayment = "WEBPAY";
        public static readonly string OverTheCounter = "OVERTHECOUNTER";
        public static readonly string Kiosk = "KIOSK";
    }
}
