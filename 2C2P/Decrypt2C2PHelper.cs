﻿using SinaptIQPKCS7;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2C2P
{
    public class Decrypt2C2PHelper
    {
        private PKCS7 sntqPKCS7 = new PKCS7();
        public string doEncrypt(string str2Encrypt, string publicKey)
        {
            string strAfterEncrypt = "";
            try
            {

                strAfterEncrypt = sntqPKCS7.encryptMessage(str2Encrypt, sntqPKCS7.getPublicCert(publicKey));
            }
            catch (Exception exp)
            {
            }
            return strAfterEncrypt;
        }


        public string doDecrypt(string str2Decrypt, string privateKey, string privateKey_pwd)
        {
            string strAfterDecrypt = "";
            try
            {
                strAfterDecrypt = sntqPKCS7.decryptMessage(str2Decrypt, sntqPKCS7.getPrivateCert(privateKey, privateKey_pwd));
            }
            catch (Exception exp)
            {

            }
            return strAfterDecrypt;
        }
    }
}
