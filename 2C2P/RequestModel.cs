﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace _2C2P
{
    public class RequestModel
    {
        public string MerchantId { get; set; } 
        public string SecretKey { get; set; } 
        public string PaymentDescription { get; set; }
        public string UniqueTransactionCode { get; set; }
        public string CurrencyCode { get; set; } = "764";
        public string Amount { get; set; }
        public string Country { get; set; } = "TH";
        public string CardHolderName { get; set; }
        public string CardData { get; set; }
        public string MaskedCardNumber { get; set; }
        public string ExpMonthCard { get; set; }
        public string ExpYearCard { get; set; }
        public string Version { get; set; } 
        public string StoredCardUniqueId { get; set; } 
        public string PaymentOption { get; set; }
        public string Option3DS { get; set; }
        public string EnableStoreCard { get; set; }
        public string payment_url { get; set; } 
        public string result_url_1 { get; set; }
        public string HashValue { get; set; }
        public string Message { get; set; }
        public string AgentCode { get; set; }
        public string ChannelCode { get; set; }
        public string PaymentExpire { get; set; }
        public string PaymentChannel { get; set; } 
        public string TimeStamp { get; set; } = DateTime.Now.ToString("ddMMyyHHmmss");
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Xml { get; set; }
        public string QrType { get; set; }



        public string SetHashValue(string Message)
        {
            string key = SecretKey;           
            var encoding = new System.Text.UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(key);
            byte[] messageBytes = encoding.GetBytes(Message);
            using (var hmacsha1 = new HMACSHA1(keyByte, false))
            {
                byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);
                HashValue = BitConverter.ToString(hmacsha1.ComputeHash(messageBytes)).Replace("-", "").ToLower();
                return HashValue;
            }
        }

        public string SetHashValueSha256(string Message)
        {            
          
            byte[] key_byte = Encoding.UTF8.GetBytes(SecretKey);
            byte[] stringToSign_byte = Encoding.UTF8.GetBytes(Message);

            //Check Signature
            HMACSHA256 hmac = new HMACSHA256(key_byte);
            byte[] hashValue = hmac.ComputeHash(stringToSign_byte);
            var result = BitConverter.ToString(hashValue).Replace("-", "");
            HashValue = result;
            return result;
        }
        
        public string SetSecureMessage()
        {
            return Version + TimeStamp + MerchantId + UniqueTransactionCode +
               PaymentDescription + Amount + CurrencyCode + PaymentChannel + Country + Email
              + Option3DS  + AgentCode + ChannelCode + PaymentExpire + MobileNo;
        }

        public string SetRedirectMessgae()
        {

            return Version + MerchantId + PaymentDescription +
                UniqueTransactionCode + CurrencyCode + Amount + result_url_1 + EnableStoreCard + StoredCardUniqueId + Option3DS + PaymentOption + QrType;
        }


    }
}
