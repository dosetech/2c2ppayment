﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2C2P
{
    public static class  PaymentRedirectOptionCode
    {
        public static readonly string AlternativePaymentMethod = "123";
        public static readonly string Alipay = "ALIPAY";
        public static readonly string AllAvailableOptions = "ALL";
        public static readonly string Bank123 = "BANK";
        public static readonly string CreditCardPayment = "CC";
        public static readonly string LinePay = "LINE";
        public static readonly string TrueMoney = "TRUEMONEY";        
        public static readonly string PayPal = "PAYPAL";
    }
    public static class PaymentSecureOptionCode
    {
        public static readonly string AlternativePaymentMethod = "123";
        public static readonly string Alipay = "ALIPAY";
        public static readonly string LinePay = "LINE";
    }
}
