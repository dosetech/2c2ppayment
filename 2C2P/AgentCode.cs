﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2C2P
{
    public class AgentCode
    {
        public static readonly string BankAyutthaya = "BAY";
        public static readonly string BankBangkok = "BBL";
        public static readonly string BankSiamCommercial = "SCB";
        public static readonly string BankKasikorn = "KBANK";
        public static readonly string BankKrungThai = "KTB";
        public static readonly string BankTMB = "TMB";
        public static readonly string BankThanachart = "TBANK";
        public static readonly string BankUOB = "UOB";
        public static readonly string Mpay = "MPAY";
        public static readonly string BigC = "BIGC";
        public static readonly string ThailandPost = "PAYATPOST";
        public static readonly string TescoLotus = "TESCO";
        public static readonly string TOT = "TOT";
        public static readonly string TrueMoney = "TRUEMONEY";
        public static readonly string Central = "CENPAY";
        public static readonly string Boonterm = "BOONTERM";
    }
}
