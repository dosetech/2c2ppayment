﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using static _2C2P.PaymentResponseSecure;

namespace _2C2P
{
    public class SecurePaymentMethod
    {
        
        public ModelResponseMsg DecodeResult(string Result,string certPath)
        {
            string privateKey = certPath;
            if (Result != "")
            {
                if (!string.IsNullOrEmpty(Result))
                {
                    string decryptedResult = new Decrypt2C2PHelper().doDecrypt(Result, privateKey, "2c2p");
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(decryptedResult);
                    string ResponseString = JsonConvert.SerializeXmlNode(doc);
                    ModelResponseMsg ResponseMsg = JsonConvert.DeserializeObject<ModelResponseMsg>(ResponseString);
                    return ResponseMsg;
                    // A Approve F Fail 

                };
                return new ModelResponseMsg();
            }
            else
            {
                ModelResponseMsg modelResponseMsg = new ModelResponseMsg();
                modelResponseMsg.PaymentResponse = new PaymentResponse { status = "error" };
                return modelResponseMsg;
            };

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="X64string"></param>
        /// <param name="certPath"></param>
        /// <returns></returns>
        public ModelResponseMsg PaymentSecue(string X64string,string certPath)
        {
            string payload = "paymentRequest=" + HttpUtility.UrlEncode(X64string);        //urlEncode data
            string url = "https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/Payment.aspx";
            string privateKey = certPath;
            if (PostRequest(payload, url, out string result, out string error))
            {
                if (!string.IsNullOrEmpty(result))
                {
                    string decryptedResult = new Decrypt2C2PHelper().doDecrypt(result, privateKey, "2c2p");
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(decryptedResult);
                    string ResponseString = JsonConvert.SerializeXmlNode(doc);
                    ModelResponseMsg ResponseMsg = JsonConvert.DeserializeObject<ModelResponseMsg>(ResponseString);
                    return ResponseMsg;
                    // A Approve F Fail 

                };
                return new ModelResponseMsg();
            }
            else
            {
                ModelResponseMsg modelResponseMsg = new ModelResponseMsg();
                modelResponseMsg.PaymentResponse = new PaymentResponse { status = error };
                return modelResponseMsg;
            };

        }
        private bool PostRequest(string request, string url, out string response, out string err)
        {
            response = "";
            err = "";
            try
            {
                //Create an instance of the WebRequest class
                WebRequest objRequest = WebRequest.Create(url);
                objRequest.Timeout = 120000; //In milliseconds - in this case 2 min
                objRequest.Method = "POST";
                objRequest.ContentLength = request.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";

                //Create an instance of the StreamWriter class and attach the WebRequest object to it - here's where we do the posting 
                StreamWriter postWriter = new StreamWriter(objRequest.GetRequestStream());
                postWriter.Write(request);
                postWriter.Close();

                //Create an instance of the WebResponse class and get the output to the rawOutput string
                WebResponse objResponse = objRequest.GetResponse();
                StreamReader sr = new StreamReader(objResponse.GetResponseStream());
                response = sr.ReadToEnd();
                sr.Close();

                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message.ToString();
                return false;
            };
        }

    }
}
