﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2C2P
{
    public class PaymentResponseRedirect
    {
        public string version { get; set; }
        public string request_timestamp { get; set; }
        public string merchant_id { get; set; }
        public string currency { get; set; }
        public string order_id { get; set; }
        public string amount { get; set; }
        public string invoice_no { get; set; }
        public string transaction_ref { get; set; }
        public string approval_code { get; set; }
        public string eci { get; set; }
        public string transaction_datetime { get; set; }
        public string payment_channel { get; set; }
        public string payment_status { get; set; }
        public string channel_response_code { get; set; }
        public string channel_response_desc { get; set; }
        public string masked_pan { get; set; }
        public string stored_card_unique_id { get; set; }
        public string backend_invoice { get; set; }
        public string paid_channel { get; set; }
        public string paid_agent { get; set; }
        public string payment_scheme { get; set; }
        public string user_defined_1 { get; set; }
        public string user_defined_2 { get; set; }
        public string user_defined_3 { get; set; }
        public string user_defined_4 { get; set; }
        public string user_defined_5 { get; set; }
        public string browser_info { get; set; }
        public string hash_value { get; set; }
        public int TransactionId { get; set; }
        public string EventAlias { get; set; }
    }
    public class PaymentResponseSecure
    {
        public class PaymentResponse
        {
            public string version { get; set; }
            public string timeStamp { get; set; }
            public string merchantID { get; set; }
            public string respCode { get; set; }
            public string pan { get; set; }
            public string amt { get; set; }
            public string uniqueTransactionCode { get; set; }
            public string tranRef { get; set; }
            public string approvalCode { get; set; }
            public string refNumber { get; set; }
            public string eci { get; set; }
            public string dateTime { get; set; }
            public string status { get; set; }
            public string failReason { get; set; }
            public string userDefined1 { get; set; }
            public string userDefined2 { get; set; }
            public string userDefined3 { get; set; }
            public string userDefined4 { get; set; }
            public string userDefined5 { get; set; }
            public string ippPeriod { get; set; }
            public string ippInterestType { get; set; }
            public string ippInterestRate { get; set; }
            public string ippMerchantAbsorbRate { get; set; }
            public string paidChannel { get; set; }
            public string paidAgent { get; set; }
            public string paymentChannel { get; set; }
            public string backendInvoice { get; set; }
            public string issuerCountry { get; set; }
            public string bankName { get; set; }
            public string processBy { get; set; }
            public string paymentScheme { get; set; }
            public string hashValue { get; set; }
        }
        public class ModelResponseMsg
        {
            public PaymentResponse PaymentResponse { get; set; }
        }
    }
}
